import React from "react";
import NewsBar from "../newsBar/newsBar";
import menuWideCss from "../../scss/mainLayout/projects/projects.scss";
import AnimatedWrapper from "../../AnimatedWrapper";

class ProjectsComponent extends React.Component{ 
	constructor(props) {
		super(props);
		this.state = {
			url: "https://webkolm.com/wp-json/wp/v2/project",
			items: []
		}
	}
   	componentDidMount() {
		let dataURL = this.state.url;
		fetch(dataURL)
			.then(res => res.json())
			.then(res => {
	           	this.setState({
	            	items: res
	           	})
        })


		

    }
	render() {
		let projects = this.state.items.map((item, index) => {
			return  <li className={"project-item project-item-" + item.id} key={index}>
						<h3 className="project-title">{item.title.rendered}</h3>
						<p className="project-content" dangerouslySetInnerHTML={{__html: item.excerpt.rendered}}></p>
					</li>
		});
		return (
			<div className="wrapper double-col parallax__group parallax">
				<div className="wrapper-left parallax__layer parallax__layer--base">
		        	<ul className={this.state.slug}>
		            	{projects}
		            	{projects}
		            	{projects}
		            	{projects}
		            	{projects}
		            	{projects}
		            	{projects}
		            	{projects}
		            	{projects}
		            	
		           	</ul>
		        </div>
		        <div className="wrapper-right parallax__layer parallax__layer--back">
		        	<NewsBar/>
		        </div>
	        </div>
       	)
    }
}

const Projects = AnimatedWrapper(ProjectsComponent);
export default Projects;