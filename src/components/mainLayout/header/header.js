import React from "react";
//import axios from "axios";
import MenuWide from "../menuWide/menuWide";
import headerCss from "../../../scss/mainLayout/header/header.scss";

export default class Header extends React.Component{ 
   constructor(props){ 
      super(props);
   }
   render(){ 
      return(
         <header>
            <div className="wrap">
               <nav className="navbar navbar-expand-lg navbar-light bg-success">
                  <MenuWide url="https://webkolm.com/wp-json/menus/v1/menus/menuwide"/>
               </nav>
            </div>
         </header>
      ); 
   }
}