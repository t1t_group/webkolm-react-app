import React from "react";
import menuWideCss from "../../../scss/mainLayout/menuWide/menuWide.scss";

export default class MenuWide extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      items: []
    }
  }
componentDidMount() {
    let dataURL = this.props.url;
    fetch(dataURL)
      .then(res => res.json())
      .then(res => {
        this.setState({
          name: res.name,
          slug: res.slug,
          items: res.items
        })
      })
  }
render() {
    let menu = this.state.items.map((item, index) => {
      return  <li className={"menu-item menu-item-" + item.ID + " " + item.classes} key={index}><a href={item.url}>{item.title}</a></li>
    });
return (
        <ul className={this.state.slug}>
          {menu}
        </ul>

        
    )
  }
}