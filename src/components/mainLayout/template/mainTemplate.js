import React from "react";

import Header from "../header/header";
import Footer from "../footer/footer";

import mainTemplate from "../../../scss/mainLayout/template/mainTemplate.scss";


export default class MainTemplate extends React.Component{ 
   constructor(props){ 
      super(props); 
   }
   render(){ 
       return( 
          
          <div> 
              <Header/>
              <div className="contenuti">
                {this.props.children} 
              </div>
         </div>
      ); 
   }
}