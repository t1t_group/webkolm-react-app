import React from "react";

import footerCss from "../../../scss/mainLayout/footer/footer.scss";

export default class Footer extends React.Component{ 
   constructor(props){ 
      super(props); 
   }
   render(){ 
      return( 
         <footer className = "container-fluid footer">
            <div className = "row">
               <div className = "col mr-auto">
                  <p>Webkolm</p>
                  <p>Via Webkolm, 56</p>
                  <p>Feltre (BL), Italia</p>
                  <p>info@webkolm.com</p>
               </div>
               <div className = "col">
                  <div className = "pull-right">
                     <a href="https://www.facebook.com/" target="_blank">
                        <i className = "fa fa-facebook-official fa-2x"></i>
                     </a>
                     <a href="https://twitter.com/" target="_blank">
                        <i className = "fa fa-twitter fa-2x"></i>
                     </a>
                  </div>
               </div>
            </div>
         </footer>
      ); 
   }
}
