import React from "react";

export default class NewsBar extends React.Component{ 
	constructor(props) {
		super(props);
		this.state = {
			url: "https://webkolm.com/wp-json/wp/v2/posts",
			items: []
		}
	}
   	componentWillMount() {
		let dataURL = this.state.url;
		fetch(dataURL)
			.then(res => res.json())
			.then(res => {
	           	this.setState({
	            	items: res
	           	})
        })
    }

    componentDidMount() {
    	let base = document.querySelector('.parallax__layer--base');
    	let back = document.querySelector('.parallax__layer--back');
    	let h1 = base.offsetHeight;
    	let h2 = back.offsetHeight;
    	let att = document.createAttribute("data-scroll-speed");       // Create a "class" attribute
    	att.value = h1/h2; 
    	console.log(h1);
    	console.log(h2);                          // Set the value of the class attribute
    	back.setAttributeNode(att);  
    }
	render() {
		let news = this.state.items.map((item, index) => {
			
			return  <li className={"project-item project-item-" + item.id} key={index}>
						<h3 className="project-title">{item.title.rendered}</h3>
						<p className="project-content" dangerouslySetInnerHTML={{__html: item.excerpt.rendered}}></p>
					</li>
		});
		return (
        	<ul className={this.state.slug}>
            	{news}
            	{news}
            	{news}
            	{news}
            	{news}
            	{news}
            	{news}
            	{news}
            	{news}
            	{news}
           	</ul>
       	)

       		
    }
}