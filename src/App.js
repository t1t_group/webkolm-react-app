import React, { Component } from 'react';
import { BrowserRouter, Route, Switch, Link} from 'react-router-dom';

import TransitionGroup from "react-transition-group/TransitionGroup";

import MainTemplate from "./components/mainLayout/template/mainTemplate";
//import Login from "./components/login/login";
import Home from "./components/home/home";
import Projects from "./components/projects/projects";
import Services from "./components/services/services";

//simport './App.scss';

const firstChild = props => {
  const childrenArray = React.Children.toArray(props.children);
  return childrenArray[0] || null;
};

class App extends Component {


  
  render() {
    return (
      <BrowserRouter>
          <MainTemplate>
              <Link to="/">Home</Link>
              <Link to="/projects">Projects</Link>
              <Link to="/services">Services</Link>
              <Route exact path="/" children={({ match, ...rest }) => (
                  <TransitionGroup component={firstChild}>
              {match && <Home {...rest}/>}
                  </TransitionGroup>
              )} />
              <Route exact path='/about-us' component={Home}/>
              <Route exact path="/projects" children={({ match, ...rest }) => (
                  <TransitionGroup component={firstChild}>
              {match && <Projects {...rest}/>}
                  </TransitionGroup>
              )} />
              <Route exact path="/services" children={({ match, ...rest }) => (
                  <TransitionGroup component={firstChild}>
              {match && <Services {...rest}/>}
                  </TransitionGroup>
              )} />
              <Route exact path='/contact-us' component={Home}/>
          </MainTemplate>
      </BrowserRouter>
    );
  }
}

export default App;
